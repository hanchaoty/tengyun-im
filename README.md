#### 一、腾云IM项目简介
+ 一直以来都有一个社交梦，想做一款IM应用，看了很多优秀的开源项目，但是没有合适自己的。于是利用休息时间自己写了这么一套系统。
+ 项目前端使用`uniapp`，后端使用`PHP`。
+ 页面设计后期会出`独立UI`。
+ 手机端使用`uniapp`实现，目前仅支持`安卓端`，后期会继续适配`iOS端`和`H5端`。
+ 您的支持，就是我们`【生发的动力】`,请手动点个`star`吧。
+ 下载体验地址：[http://im.hanchaotangyue.net/member_register/#/pages/h5/download](http://im.hanchaotangyue.net/member_register/#/pages/h5/download)
+ 插件市场 https://ext.dcloud.net.cn/plugin?id=10602
#### 二、使用须知
+ 仅允许`技术学习`使用
+ 不允许`本产品及其衍生品`进行任何形式`商业使用`
+ 请自觉遵守本开源协议（MPL-2.0），再次开源请注明出处
+ 推荐Watch、Star项目，获取项目第一时间更新，同时也是对项目最好的支持
+ 希望大家多多支持本开源作品
+ 开源的为前端需要配和腾云im后端使用.需要后端源码可以联络下方客服

#### 三、技术使用
+ 推送：uniPush1.0 或 uniPush2.0
+ 资源：阿里OSS/七牛云（图片、声音、视频、文件等）
+ 地图：高德地图
+ 短信：阿里云短信,凌凯短信
+ 后端：thinkphp5.1
+ 前端：uniapp(Vue2)

#### 四、功能介绍
+ 涵盖的模块有：登录、通讯录、群聊、单聊、消息、我的等；
+ 涉及的功能有：创建群聊、成员管理、删除/退出群聊、设置群名称和头像；添加好友；个人头像等；
+ 消息类型有：文本、表情、图片、语音、位置、文件等，你还可以扩展其他消息类型。
+ 特色功能：支持持好友圈点赞以及素材圈分享

#### 五、DEMO体验的说明
1.默认选择是uniPush1.0 仅支持app,如果需要h5需要 选择开通配置unipush2.0 并选择
然后在app.vue中设置 globalData.pushversion=2 , 解压根目录下uniCloud-aliyun.zip 关联云空间并部署

2.其它配置在\common\config.js

3.支持对接第三方应用,通过api同步会员 通过token自动登录app 可自由扩展其它功能
``` 
独立版本:imchat-tengyun://?token=a2c6bb1ef5ea730cb5bc79d3229cbb7dc3e7f572
集成版本:this.$fc.loginwithtoken(token);
```
4.也支持以分包形式集成到现有的uniapp项目中.默认分包为chat 也可修改分包名称

5.也可联系官方体验

![图片](https://www.hanchao9999.com/Application/Home/View/default/images/ewm.jpg)

