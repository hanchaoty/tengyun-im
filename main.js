import http from '@/common/request'
import store from './store'
import md5 from "@/common/md5.js";
import publicFc from "@/common/publicFc.js";
import zmmFormCheck from './common/zmmFormCheck.js';
import pinyin from './common/pinyin.js';
// #ifndef VUE3
import Vue from 'vue'
import App from './App'
Vue.config.productionTip = false
Vue.prototype.$http = http
Vue.prototype.$store = store
Vue.prototype.$fc = publicFc;
Vue.prototype.$md5 = md5
Vue.prototype.$zmmFormCheck = zmmFormCheck;
Vue.prototype.$pinyin = pinyin;
App.mpType = 'app'
const app = new Vue({
    ...App,
	store
})
app.$mount()
// #endif

// #ifdef VUE3
import { createSSRApp } from 'vue'
import App from './App.vue'
export function createApp() {
  const app = createSSRApp(App)
  app.config.globalProperties.$http = http
  app.config.globalProperties.$fc = publicFc
  app.config.globalProperties.$md5 = md5
  app.config.globalProperties.$zmmFormCheck = zmmFormCheck
  app.config.globalProperties.$pinyin = pinyin
  app.use(store);
  return {
    app
  }
}
// #endif
// #ifdef APP-PLUS
	// if(plus.os.name == 'Android'){
	// 	var main = plus.android.runtimeMainActivity();
	// 	//为了防止快速点按返回键导致程序退出重写quit方法改为隐藏至后台  
	// 	plus.runtime.quit = function() {
	// 	    main.moveTaskToBack(false);
	// 	};
	// 	//重写toast方法如果内容为 ‘再次返回退出应用’ 就隐藏应用，其他正常toast
	// 	plus.nativeUI.toast = (function(str) {
	// 	    if (str =='再次返回退出应用') {
	// 	        plus.runtime.quit();
	// 	    } else {
	// 	        uni.showToast({
	// 	            title: '再次返回退出应用',
	// 	            icon: 'none'
	// 	        })
	// 	    }
	// 	});
	// }
// #endif